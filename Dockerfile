FROM docker.io/library/golang:1.16 AS compiler

ENV CGO_ENABLED 0
WORKDIR /go/src/gitlab.com/oss-cloud/k8s-oidc-login

COPY . .

RUN go build -v .

FROM gcr.io/distroless/static:latest

COPY --from=compiler /go/src/gitlab.com/oss-cloud/k8s-oidc-login/k8s-oidc-login /k8s-oidc-login

CMD ["/k8s-oidc-login"]
