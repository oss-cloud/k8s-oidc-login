# Kubernetes OIDC Login

This is for helping login with OIDC and generate kubeconfig file

| Environment Variable | Usage | Required |
|----------------------|-------|----------|
| OIDC_ISSUER | issuer of OIDC provider (e.g. https://accounts.google.com/) | yes |
| OIDC_CLIENT_ID | Client ID | yes |
| OIDC_CLIENT_SECRET | Client Secret | yes |
| OIDC_CA_ACCEPT | OIDC provider is using custom CA, 1 for true and 0 for false | no (default 0) |
| OIDC_CA_PATH | CA path of OIDC provider | no |
| OIDC_REDIRECT_HOSTNAME | hostname of this server | yes |
| OIDC_SCOPES | scopes of OIDC provider | no (default: offline_access) |
| LOG_LEVEL | level of logging | no (default: info) |
| K8S_APISERVER_ENDPOINT | Kubernetes Apiserver Endpoint for user to access | yes |
| K8S_CA_PATH | Path to Kubernetes CA | yes |
