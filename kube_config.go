package main

import (
	"golang.org/x/oauth2"
	"gopkg.in/yaml.v2"
)

func createKubeConfigYaml(k8sCert, oidcCert string, t *oauth2.Token) ([]byte, error) {
	kubeConfg := Config{
		ApiVersion:     "v1",
		Kind:           "Config",
		CurrentContext: "kubernetes",
		Contexts: []Context{
			{
				Name: "kubernetes",
				Context: ContextSpec{
					Cluster: "kubernetes",
					User:    "oidc",
				},
			},
		},
		Clusters: []Cluster{
			{
				Name: "kubernetes",
				Cluster: ClusterSpec{
					Server: k8sApiserverEndpoint,
					CaData: k8sCert,
				},
			},
		},
		Users: []User{
			{
				Name: "oidc",
				User: UserSpec{
					AuthProvider: AuthProvider{
						Name: "oidc",
						Config: AuthProviderConfig{
							Issuer:       oidcIssuer,
							CaData:       oidcCert,
							ClientId:     oidcClientId,
							ClientSecret: oidcClientSecret,
							ExtraScopes:  oidcScopes,
							IdToken:      t.Extra("id_token").(string),
							RefreshToken: t.RefreshToken,
						},
					},
				},
			},
		},
	}
	return yaml.Marshal(&kubeConfg)
}

type Config struct {
	ApiVersion     string    `yaml:"apiVersion"`
	Kind           string    `yaml:"kind"`
	CurrentContext string    `yaml:"current-context"`
	Contexts       []Context `yaml:"contexts"`
	Clusters       []Cluster `yaml:"clusters"`
	Users          []User    `yaml:"users"`
}

type Context struct {
	Name    string      `yaml:"name"`
	Context ContextSpec `yaml:"context"`
}

type ContextSpec struct {
	Cluster string `yaml:"cluster"`
	User    string `yaml:"user"`
}

type Cluster struct {
	Name    string      `yaml:"name"`
	Cluster ClusterSpec `yaml:"cluster"`
}

type ClusterSpec struct {
	Server string `yaml:"server"`
	CaData string `yaml:"certificate-authority-data"`
}

type User struct {
	Name string   `yaml:"name"`
	User UserSpec `yaml:"user"`
}

type UserSpec struct {
	AuthProvider AuthProvider `yaml:"auth-provider"`
}

type AuthProvider struct {
	Name   string             `yaml:"name"`
	Config AuthProviderConfig `yaml:"config"`
}

type AuthProviderConfig struct {
	Issuer       string `yaml:"idp-issuer-url"`
	CaData       string `yaml:"idp-certificate-authority-data,omitempty"`
	ClientId     string `yaml:"client-id"`
	ClientSecret string `yaml:"client-secret"`
	ExtraScopes  string `yaml:"extra-scopes"`
	IdToken      string `yaml:"id-token"`
	RefreshToken string `yaml:"refresh-token"`
}
