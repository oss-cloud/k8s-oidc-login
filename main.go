package main

import (
	"context"
	"crypto/tls"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strings"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"golang.org/x/oauth2"
)

func getEnvWithFallback(key string, fallback string) string {
	if value := os.Getenv(key); value != "" {
		return value
	}

	if fallback == "" {
		panic(fmt.Sprintf("Environment variable %q is required", key))
	}

	return fallback
}

var (
	port                 = getEnvWithFallback("PORT", "8080")
	oidcIssuer           = getEnvWithFallback("OIDC_ISSUER", "")
	oidcClientId         = getEnvWithFallback("OIDC_CLIENT_ID", "")
	oidcClientSecret     = getEnvWithFallback("OIDC_CLIENT_SECRET", "")
	oidcCaAccept         = getEnvWithFallback("OIDC_CA_ACCEPT", "0")
	oidcRedirectHostname = getEnvWithFallback("OIDC_REDIRECT_HOSTNAME", "")
	oidcScopes           = getEnvWithFallback("OIDC_SCOPES", "offline_access")
	logLevel             = getEnvWithFallback("LOG_LEVEL", "info")
	k8sApiserverEndpoint = getEnvWithFallback("K8S_APISERVER_ENDPOINT", "")
	k8sCaPath            = getEnvWithFallback("K8S_CA_PATH", "")
)

func getCaPathData(path string) (_ string, err error) {
	pemData, err := ioutil.ReadFile(path)
	if err != nil {
		return
	}
	return base64.StdEncoding.EncodeToString(pemData), nil
}

func newLogger() *zap.Logger {
	config := zap.NewDevelopmentConfig()
	var level zapcore.Level
	if err := level.UnmarshalText([]byte(logLevel)); err != nil {
		panic(err)
	}
	config.Level = zap.NewAtomicLevelAt(level)
	logger, err := config.Build()
	if err != nil {
		panic(err)
	}
	return logger
}

func createOAuth2Config(client *http.Client, logger *zap.Logger) (*oauth2.Config, error) {
	wellKnownUrl := strings.TrimSuffix(oidcIssuer, "/") + "/.well-known/openid-configuration"
	req, err := http.NewRequest(http.MethodGet, wellKnownUrl, nil)
	if err != nil {
		logger.With(zap.Error(err)).Fatal("fail to create request for OIDC well known url")
	}
	res, err := client.Do(req)
	if err != nil {
		logger.With(zap.Error(err)).Fatal("error occur during fetch OIDC dynamic configuration")
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		_, _ = io.Copy(io.Discard, res.Body)
		logger.With(zap.Int("status", res.StatusCode)).Fatal("invalid status code response")
	}
	var claim struct {
		AuthURL  string `json:"authorization_endpoint"`
		TokenURL string `json:"token_endpoint"`
	}
	if err := json.NewDecoder(res.Body).Decode(&claim); err != nil {
		logger.With(zap.Error(err)).Fatal("fail to parse OIDC dynamic configuration")
	}

	oidcRedirectUrl := strings.TrimSuffix(oidcRedirectHostname, "/") + "/callback"

	config := oauth2.Config{
		ClientID:     oidcClientId,
		ClientSecret: oidcClientSecret,
		Endpoint: oauth2.Endpoint{
			AuthURL:   claim.AuthURL,
			TokenURL:  claim.TokenURL,
			AuthStyle: oauth2.AuthStyleInHeader,
		},
		RedirectURL: oidcRedirectUrl,
		Scopes:      append(strings.Split(oidcScopes, ","), "openid", "email", "profile"),
	}
	return &config, nil
}

func main() {
	logger := newLogger()

	client := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: oidcCaAccept == "1",
			},
		},
	}

	config, err := createOAuth2Config(client, logger)
	if err != nil {
		return
	}

	oidcCert := ""
	if oidcCaAccept == "1" {
		oidcCaPath := getEnvWithFallback("OIDC_CA_PATH", "")
		oidcCert, err = getCaPathData(oidcCaPath)
		if err != nil {
			logger.With(zap.Error(err)).Fatal("error occur during fetching oidc ca cert")
		}
	}

	k8sCert, err := getCaPathData(k8sCaPath)
	if err != nil {
		logger.With(zap.Error(err)).Fatal("error occur during fetching k8s ca cert")
	}

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Location", config.AuthCodeURL(""))
		w.WriteHeader(http.StatusFound)
	})

	http.HandleFunc("/callback", func(w http.ResponseWriter, r *http.Request) {
		ctx := context.WithValue(r.Context(), oauth2.HTTPClient, client)
		t, err := config.Exchange(ctx, r.URL.Query().Get("code"))
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			logger.With(zap.Error(err)).Error("error occur during exchange token")
			_, _ = w.Write([]byte("internal error occur"))
			return
		}

		result, err := createKubeConfigYaml(k8sCert, oidcCert, t)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			logger.With(zap.Error(err)).Error("error occur during encode kubeconfig")
			_, _ = w.Write([]byte("internal error occur"))
			return
		}
		fileUrl := base64.StdEncoding.EncodeToString(result)
		w.Header().Set("Content-Type", "text/html; charset=UTF-8")
		w.Write([]byte(`
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Login Success</title>
<style> body { font-family: 'Roboto', Arial, sans-serif; font-size: 16px; }</style>
</head>
<body>
<h1>Login Success</h1>
<p>Please save below as a file <a target="_blank" download="kubeconfig.yaml" href="data:text/plain;base64,` + fileUrl + `">kubeconfig</a> file and use it to access the cluster (<a href="https://kubernetes.io/docs/tasks/access-application-cluster/configure-access-multiple-clusters/#set-the-kubeconfig-environment-variable" target="_blank">Guide</a>)
`))
	})
	logger.With(zap.String("port", port)).Info("start listening")
	http.ListenAndServe(":"+port, nil)
}
